# Change Log (node-enumerable)

## 1.2.0 (May 22nd, 2017; sort functions)

* added `sort()` and `sortDesc()` functions

## 1.1.0 (May 22nd, 2017; distinctBy)

* added `distinctBy()` method for [IEnumerable](https://mkloubert.github.io/node-enumerable/interfaces/_index_.enumerable.ienumerable.html)

## 1.0.0 (May 22nd, 2017; inital release)

* first stable release
